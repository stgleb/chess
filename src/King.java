import java.util.List;

/**
 * Created by gstepanov on 08.09.14.
 */
public class King extends Figure {
    public King() {
        directions.add(new Direction(1, 1, 1));
        directions.add(new Direction(1, -1, 1));
        directions.add(new Direction(-1, 1, 1));
        directions.add(new Direction(-1, -1, 1));

        directions.add(new Direction(0, 1, 1));
        directions.add(new Direction(0, -1, 1));
        directions.add(new Direction(-1,0, 1));
        directions.add(new Direction(1, 0, 1));

    }

    @Override
    public List<Direction> getDirections() {
        return directions;
    }
}
