
public class Direction {
    public int dX;

    public int dY;

    public int distance;

    public Direction(int dx,int dy,int distance) {
        this.dX = dx;
        this.dY = dy;
        this.distance = distance;
    }
}
