import java.util.List;

/**
 * Created by gstepanov on 08.09.14.
 */
public class Bishop extends Figure {
    public Bishop() {
        directions.add(new Direction(1, 1, 8));
        directions.add(new Direction(1, -1, 8));
        directions.add(new Direction(-1, 1, 8));
        directions.add(new Direction(-1, -1, 8));
    }

    @Override
    public List<Direction> getDirections() {
        return directions;
    }
}
