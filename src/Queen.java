import java.util.List;

public class Queen extends Figure {
    public Queen() {
        directions.add(new Direction(1, 1, 8));
        directions.add(new Direction(1, -1, 8));
        directions.add(new Direction(-1, 1, 8));
        directions.add(new Direction(-1, -1, 8));

        directions.add(new Direction(1, 0, 8));
        directions.add(new Direction(-1, 0, 8));
        directions.add(new Direction(0, 1, 8));
        directions.add(new Direction(0, -1, 8));
    }

    @Override
    public List<Direction> getDirections() {
        return directions;
    }
}
