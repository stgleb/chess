import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class Main {

    private static int counter = 0;
    public static Map<String,Boolean> ans = new HashMap<String, Boolean>();


    //Function returns proper class by the char
    public static Figure getFigure(char c) {
        switch (c) {
            case 'P': return new Pawn();

            case 'B': return new Bishop();

            case 'R': return new Rook();

            case 'k': return new Knight();

            case  'K': return new King();

            case 'Q': return new Queen();

            default:break;
        }

        return null;
    }

    //Check whether all figures were used or not.

    public static boolean checkFigures(Map<Character,Integer> figures) {
        for(Character c : figures.keySet()) {
            if(figures.get(c) != 0) {
                return false;
            }
        }

        return true;
    }

    //Main recursive function that backtracks solution tree.

    public static void backtracking(Desk desk,Map<Character,Integer> figures, int xBeg, int yBeg) {

        for(Character c : figures.keySet()) {

            if(figures.get(c) > 0) {
                figures.put(c,figures.get(c) - 1);

                for(int i = 0;i < desk.getSize();i++) {
                    for(int j = 0;j < desk.getSize();j++) {

                        Figure f = getFigure(c);
                        if (desk.getStatus(i, j) == Constants.FREE_CELL && desk.putOnDesk(f,i,j)) {
                            backtracking(desk,figures,i,j);

                            if(checkFigures(figures)) {
                                ans.put(desk.printState(), true);
                            }

                            desk.removeFromDesk(f, i, j);
                        }
                    }
                }

                figures.put(c, figures.get(c) + 1);
            }
        }
    }

    //reading data from the file.
    public static int readData(Map<Character,Integer> map,String fileName) throws IOException {
        FileReader fin = new FileReader("data.txt");
        Scanner src = new Scanner(fin);

        int n = src.hasNextInt() ? src.nextInt() : 0;

        while (src.hasNext()){

            char c = src.next().charAt(0);
            int count = src.nextInt();

            if(map.get(c) == null) {
                counter++;
            }
            map.put(c,count);

        }

        return n;
    }

    public static void main(String[] args) throws InterruptedException, IOException {
        Map<Character,Integer> map = new HashMap<Character, Integer>();
        int size = readData(map, "data.txt");

        Desk desk = new Desk(size);
        backtracking(desk,map,0,0);

        for(String s: ans.keySet()) {
            System.out.println(s);
        }

        System.out.println(ans.keySet().size());
    }
}

