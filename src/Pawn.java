import java.util.List;

/**
 * Created by gstepanov on 08.09.14.
 */
public class Pawn extends Figure {

    public Pawn() {
        directions.add(new Direction(1, -1, 1));
        directions.add(new Direction(-1, -1, 1));
    }

    @Override
    public List<Direction> getDirections() {
        return directions;
    }
}
