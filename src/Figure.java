import java.util.ArrayList;
import java.util.List;

public abstract class Figure {

    protected List<Direction> directions = new ArrayList<Direction>();

    public abstract List<Direction> getDirections();
}
