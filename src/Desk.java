import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class Desk {

    private int[][] desk;
    private int size;
    private Map<Point,Figure> figureMap = new HashMap<Point, Figure>();
    private Map<Class,Character> reverseMap = new HashMap<Class, Character>();

    public Desk(int size) {
        desk = new int[size][];
        this.size = size;

        for(int i = 0;i < size;i++) {
            desk[i] = new int[size];
        }

        reverseMap.put(Pawn.class,'P');
        reverseMap.put(Bishop.class,'B');
        reverseMap.put(Knight.class,'k');
        reverseMap.put(Rook.class,'R');
        reverseMap.put(Queen.class,'Q');
        reverseMap.put(King.class,'K');


    }

    public boolean putOnDesk(Figure f,int x,int y) {
        if(desk[x][y] != Constants.FREE_CELL) {
            return false;
        }

        //Check if position is available.
        for(Direction d:f.getDirections()) {
            int curX = x + d.dX;
            int curY = y + d.dY;
            int count = d.distance;

            boolean flag = false;

            while ((curX >= 0 && curX < size) && (curY >= 0 && curY < size) && (count > 0)){
                if(desk[curX][curY] == Constants.ENGAGED) {
                    return false;
                }
                curX += d.dX;
                curY += d.dY;
                count--;
            }
        }
        //Set figure on position
        desk[x][y] = Constants.ENGAGED;
        figureMap.put(new Point(x,y),f);

        for(Direction d:f.getDirections()) {
            int curX = x + d.dX;
            int curY = y + d.dY;
            int count = d.distance;

            boolean flag = false;

            while ((curX >= 0 && curX < size) && (curY >= 0 && curY < size)  && (count > 0)) {
                desk[curX][curY] += Constants.UNDER_ATTACK;
                curX += d.dX;
                curY += d.dY;
                count--;
            }
        }

        return true;
    }

    public void removeFromDesk(Figure f,int x,int y) {
        //clear field

        desk[x][y] = Constants.FREE_CELL;
        figureMap.remove(new Point(x, y));

        for(Direction d:f.getDirections()) {
            int curX = x + d.dX;
            int curY = y + d.dY;
            int count = d.distance;

            boolean flag = false;

            while ((curX >= 0 && curX < size) && (curY >= 0 && curY < size) && (count > 0)) {
                desk[curX][curY] -= Constants.UNDER_ATTACK;
                curX += d.dX;
                curY += d.dY;
                count--;
            }
        }

    }

    public int getStatus(int x,int y) {
        if(x > size || y > size) {
            throw  new IndexOutOfBoundsException("Desk index out of bound");
        }

        return desk[x][y];
    }

    public int getSize() {
        return size;
    }

    public String printState() {
        StringBuilder sb = new StringBuilder();

        for(int i = 0;i < size;i++) {
            for(int j = 0;j < size;j++) {
                if(desk[i][j] == Constants.FREE_CELL) {
                    sb.append('*');
                } else {
                    if(desk[i][j] == Constants.ENGAGED) {
                        Figure f = figureMap.get(new Point(i,j));
                        sb.append(reverseMap.get(f.getClass()));
                    } else {
                        sb.append('*');
                    }
                }
            }
            sb.append('\n');
        }

        return sb.toString();
    }

}
