import java.util.List;

/**
 * Created by gstepanov on 08.09.14.
 */
public class Rook extends Figure {
    public Rook() {
        directions.add(new Direction(1, 0, 8));
        directions.add(new Direction(-1, 0, 8));
        directions.add(new Direction(0, 1, 8));
        directions.add(new Direction(0, -1, 8));
    }

    @Override
    public List<Direction> getDirections() {
        return directions;

    }
}
