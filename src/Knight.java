import java.util.List;

public class Knight  extends Figure{
    public Knight() {
        directions.add(new Direction(1, -2, 1));
        directions.add(new Direction(1, 2, 1));
        directions.add(new Direction(-1, -2, 1));
        directions.add(new Direction(-1, 2, 1));
        directions.add(new Direction(2, -1, 1));
        directions.add(new Direction(2, 1, 1));
        directions.add(new Direction(-2, -1, 1));
        directions.add(new Direction(-2, 1, 1));
    }

    @Override
    public List<Direction> getDirections() {
        return directions;
    }
}
